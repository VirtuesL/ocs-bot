import {Client} from 'discord.js';
import {BanchoClient} from 'bancho.js';

import {discord, bancho } from './config.json';
const DiscordClient: Client = new Client();
const BanClient:BanchoClient = new BanchoClient({username: bancho.username , password: bancho.password});


BanClient.connect().then(()=>{
  console.log("Bancho Started");
})

DiscordClient.on("ready", () => {
  console.log(`Logged in as ${DiscordClient.user!.tag}`);
  
});

DiscordClient.on("message", message => {
  if (message.author.bot) return;
  if (!message.content.startsWith("£")) return;
})


DiscordClient.login(discord.token)
